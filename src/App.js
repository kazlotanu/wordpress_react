import React from "react";
import { Table } from "react-bootstrap";
import { useEffect, useState } from "react";
import WooCommerceRestApi from "@woocommerce/woocommerce-rest-api";
import moment from "moment";

const api = new WooCommerceRestApi({
  url: "https://kazlo12-urbvb.cloud.mn",
  consumerKey: "ck_36e48f94d3f2f637dfb2668c26c4b0ad6688ed63",
  consumerSecret: "cs_58c370e1f452a1b5a8a32461750b414dd2a3c63c",
  version: "wc/v3"
});

function App() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetchOrders();
  }, []);

  let fetchOrders = () => {
    api
      .get("products", {
        // .get("orders", {
        per_page: 20
      })
      .then((response) => {
        if (response.status === 200) {
          console.log(response.data, "ddd");
          setOrders(response.data);
        }
      })
      .catch((error) => {});
  };

  return (
    <div className="App">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Date</th>
            <th>Amount</th>
            <th>Name</th>
            <th>Email</th>
            <th>Adress</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order, index) => {
            return (
              <tr key = {index}>
                <td> {order.id} </td> 
                <td > {moment(order.date).format("DD-MM-YYYY")} </td> 
                <td > {order.name} </td> 
                <td > {order.date_created && order.date_created} </td> 
                <td > {order.permalink && order.permalink} </td> 
                <td > {order.billing && order.billing.address_1} </td> 
                </tr>
            );
          })}
        </tbody>
        <tfoot>
          <tr>
            <th>#</th>
            <th>Date</th>
            <th>Amount</th>
            <th>Name</th>
            <th>Email</th>
            <th>Adress</th>
          </tr>
        </tfoot>
      </Table>
    </div>
  );
}

export default App;
